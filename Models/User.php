<?php
class User
{
    private PDO|null $connection;

    /**
     * @throws Exception
     */
    public function __construct(Database $db)
    {
        $this->connection = $db->get_connection();
    }

    /**
     * @throws Exception if there is an error querying the database,
     *  if the user doesn't exist or there are no roles for the user
     */
    public function get_user_role(string $username, string $password) : array
    {
        try {
            $user_id = $this->get_user_id($username, $password);
            $query = "SELECT p.role FROM user_permissions up
            JOIN users AS u ON u.id = up.user_id
            LEFT JOIN permissions AS p ON p.id = up.permission_id
            WHERE u.id = $user_id";

            if($user_id > 0){
                $role = $this->connection->query($query)->fetchAll(PDO::FETCH_COLUMN);
                if(!empty($role)){
                    return $role;
                } else {
                    throw new Exception("No roles for this user");
                }
            } else {
                throw new Exception("User not found");
            }

        } catch (PDOException $exception) {
            throw new Exception("Error getting role".$exception->getMessage());
        }

    }

    /**
     * @throws Exception if there is an error querying the database
     * or the user doesn't exist
     */
    public function get_user_id(string $username, string $password) : int
    {
        try {
            $query = "SELECT id FROM users u
            WHERE u.username = ?
            AND u.password = ?";
            $prepare = $this->connection->prepare($query);
            $prepare->execute([$username,$password]);
            return (int)$prepare->fetchColumn();
        } catch (PDOException $exception){
            throw new Exception("Error getting user id".$exception->getMessage());
        }
    }
    /**
     * @throws Exception if there is an error querying the database
     *  or the user id doesn't match
     */
    public function get_user_name(int $id) : string{
        try {
            $query = "SELECT name FROM users u 
                WHERE u.id = ?";
            $prepare = $this->connection->prepare($query);
            $prepare->execute([$id]);
            return (string)$prepare->fetchColumn();
        } catch (PDOException $exception){
            throw new Exception("Error getting user name".$exception->getMessage());
        }
    }
    /**
     * @throws Exception if there is an error querying the database
     *   or the user doesn't have tickets
     */
    public function get_user_tickets(int $id) : array{
        try {
            $query = "SELECT s.id,o.city as origin, d.city as destination, t.departure_date, t.departure_time, t.arrival_date, t.arrival_time, t.price FROM sales s
                JOIN tickets t ON t.id = s.tickets_id
                JOIN origin o ON o.id = t.origin_id
                JOIN destination d ON d.id = t.destination_id
                WHERE s.user_id = ?";
            $prepare = $this->connection->prepare($query);
            $prepare->execute([$id]);
            return $prepare->fetchAll();
        } catch (PDOException $exception){
            throw new Exception("Error getting user tickets".$exception->getMessage());
        }
    }
    /**
     * @throws Exception if there is an error querying the database
     *   or the ticket doesn't exist
     */
    public function cancel_ticket(int $ticked_id): bool
    {
        try {
            $query = "DELETE FROM sales
                WHERE sales.id = ?";
            $prepare = $this->connection->prepare($query);
            return $prepare->execute([$ticked_id]);
        } catch (Exception $e){
            throw new Exception("Error deleting user tickets".$exception->getMessage());
        }
    }
}