<?php

class Database extends Connection
{
    /**
     * @param string $tableName (origin or destination)
     * @return array|null if cities were found or there are no cities
     * @throws Exception if there is an error querying the database
     */
    public function get_cities(string $tableName) : ?array
    {
        try {
            $allowed = ['origin','destination'];
            if(in_array($tableName,$allowed)){
                $query = "SELECT city FROM $tableName";
                return $this->get_connection()->query($query)->fetchAll();
            } else {
                return null;
            }
        } catch (PDOException $exception) {
            throw new Exception("Error retrieving cities:".$exception->getMessage());
        }
    }
    /**
     * @param string $origin origin table
     * @param string $destination destination table
     * @param int $tickets number of tickets needed
     * @param string $departure_date date of tickets
     * @return array with tickets or without
     * @throws Exception when tickets cannot be retrieved due db error
     */
    public function get_tickets(string $origin, string $destination, int $tickets, string $departure_date) : array
    {
        try {
            // Esta consulta selecciona todas las columnas de tickets y las columnas
            // capacidad y tipo de la tabla vehiculos.
            // Mediante el uso de JOIN unimos las tablas origin y destination
            // que tengan el mismo ID del ticket con ese origin, destination y departure (WHERE)
            // También, añadimos la tabla vehículos de acuerdo al ID del vehículo especificado
            // y las ventas mediante el mismo id del ticket.
            // Luego se agrupan para obtener la capacidad restante de cada combinación
            // y se filtran restando a la capacidad del vehículo los tickets vendidos
            // NOTA: Es importante destacar que se debe verificar que en la resta
            // COUNT no sea nulo, para ello se usa COALESCE equivalente a IFNULL
            // que verifica si los valores son null, y retorna el siguiente
            // de esta forma la consulta se va a ejecutar correctamente
            // cuando no haya ventas relacionadas en el JOIN ventas
            $query = "SELECT p.*,v.type ,v.capacity - COALESCE(COUNT(s.id), 0) AS available_capacity
            FROM tickets p
            JOIN origin o ON p.origin_id = o.id
            JOIN destination d ON p.destination_id = d.id
            JOIN vehicles v ON p.vehicle_id = v.id
            LEFT JOIN sales s ON s.tickets_id = p.id
            WHERE o.city = '$origin'
            AND d.city = '$destination'
            AND p.departure_date = '$departure_date'
            GROUP BY p.id, v.type, v.id
            HAVING v.capacity - COALESCE(COUNT(s.id), 0) >= $tickets";
            return $this->get_connection()->query($query)->fetchAll();
        } catch (PDOException $exception) {
            throw new Exception("Error retrieving tickets".$exception->getMessage());
        }
    }
    /**
     * Unused, only if needed in the future
     * @param int $ticket_id receives id from tickets table
     * @param int $vehicle_id receives vehiculo_id from tickets table
     * @return int available tickets
     * @throws Exception if there is an error querying the database
    */
    public function get_available_tickets(int $ticket_id, int $vehicle_id) : int
    {
        try {
            $query = "SELECT v.capacity - COALESCE(COUNT(s.id), 0) AS availables
            FROM tickets p
            JOIN vehicles v ON p.vehicle_id = $vehicle_id
            LEFT JOIN sales s ON s.tickets_id = $ticket_id
            WHERE p.id = $ticket_id
            GROUP BY v.capacity";
            return (int)$this->get_connection()->query($query)->fetchColumn();
        } catch (PDOException $exception) {
            throw new Exception("Error retrieving tickets".$exception->getMessage());
        }
    }

    public function close(): void
    {
        $this->close_connection();
    }

    public function get_customer_id(int $dni) : int
    {
        try {
            $query = "SELECT id FROM passengers WHERE dni = $dni";
            return (int)$this->get_connection()->query($query)->fetchColumn();
        } catch (PDOException $exception) {
            die("Error: " . $exception->getMessage());
        }
    }

    /**
     * @return false if customer is already loaded
     * @return true if customer is properly loaded
     * @throws Exception if there is an error querying the database
     */
    public function insert_customer(string $name, string $lastname, int $dni, string $gender, string $birth) : bool
    {
        try {
            $id_exists = $this->get_customer_id($dni);
            if($id_exists > 0){
                return false;
            }
            $query = "INSERT INTO passengers (name, lastname, dni, gender, birth) VALUES (?, ?, ?, ?, ?)";
            $prepare = $this->get_connection()->prepare($query);
            $prepare->execute([strtolower($name), strtolower($lastname), $dni, $gender, $birth]);
            return true;
        } catch (PDOException $exception) {
            throw new Exception("Error loading customers".$exception->getMessage());
        }
    }
    /**
     * @param int $customer_id id from customers table
     * @param int $ticket_id id from tickets table
     * @return bool whether the sale was loaded or not
     * @throws Exception if there is an error querying the database
    */
    public function insert_sales(int $customer_id, int $ticket_id) : bool
    {
        try {
            $query = "INSERT INTO sales (tickets_id, user_id) VALUES (?, ?)";
            $result = $this->get_connection()->prepare($query);
            return $result->execute([$ticket_id, $customer_id]);
        } catch (PDOException $exception) {
            throw new Exception("Error inserting sale".$exception->getMessage());
        }
    }
    /**
     * @param int $ticket_id id from tickets table
     * @return int price from ticket
     * @throws Exception if there is an error querying the database
    */

    public function get_price(int $ticket_id) : int
    {
        try {
            $query = "SELECT price FROM tickets WHERE id = $ticket_id";
            return (int)$this->get_connection()->query($query)->fetchColumn();
        } catch (PDOException $exception) {
            throw new Exception("Error getting price".$exception->getMessage());
        }
    }
    /**
     * @return bool if data matches with customers table
     * @throws Exception if there is an error querying the database
    */
    public function client_data_matches(string $name, string $lastname, int $dni, string $gender, string $birth) : bool
    {
        try {
            $query = "SELECT COUNT(*) FROM passengers c
                WHERE c.name = ?
                AND c.lastname = ?
                AND c.dni = ?
                AND c.gender = ?
                AND c.birth = ?";
            $result = $this->get_connection()->prepare($query);
            $result->execute([$name,$lastname,$dni,$gender,$birth]);
            $success = $result->fetchColumn();
            return $success >= 1;
        } catch (PDOException $exception) {
            throw new Exception("Failed to match data".$exception->getMessage());
        }
    }

}