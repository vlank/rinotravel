<?php

use Dotenv\Dotenv;

include_once dirname(__DIR__) . '/vendor/autoload.php';


class Connection
{
    protected PDO|null $connection;

    private string $dbms, $host, $database, $username, $password;

    public function __construct()
    {
        $dotenv = Dotenv::createMutable(dirname(__DIR__));
        $dotenv->load();
        $this->dbms = "pgsql";
        $this->host = $_ENV['HOST'];
        $this->database = $_ENV['DATABASE'];
        $this->username = $_ENV['USERNAME'];
        $this->password = $_ENV['PASSWORD'];
        $this->connect();
    }
    /**
     * @throws Exception if there is an error with database connection
     */
    private function connect(): void
    {
        try {
            $this->connection = new PDO("$this->dbms:host=$this->host;dbname=$this->database", $this->username, $this->password,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8"));
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {
            throw new Exception("Error connecting to database:" . $exception->getMessage());
        }
    }

    public function get_connection(): PDO|null
    {
        return $this->connection;
    }
    public function close_connection() : void
    {
        $this->connection = null;
    }
}