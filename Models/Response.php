<?php

class Response
{
    const OK = 200;
    const CREATED = 201;
    const NO_CONTENT = 204;
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const METHOD_NOT_ALLOWED = 405;
    const INTERNAL_SERVER_ERROR = 500;
    const SERVICE_UNAVAILABLE = 503;
    public static function getMessage(int $code = 404) : string{
        switch ($code){
            case 200 : {
                return 'Request successful';
            }
            case 201: {
                return 'Resource successfully created';
            }
            case 204 : {
                return 'Request successful, but no response body';
            }
            case 400 :{
                return 'Malformed request';
            }
            case 401 : {
                return 'Authentication needed';
            }
            case 403 : {
                return 'Access to the resource is denied';
            }
            case 405 : {
                return 'HTTP method not supported for this resource';
            }
            case 500 : {
                return 'Server encountered an error processing the request';
            }
            case 503: {
                return 'The service is temporarily unavailable';
            }
            default : {
                return 'Resource not found';
            }
        }
    }
}