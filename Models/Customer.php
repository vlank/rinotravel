<?php

class Customer
{
    private string $name;
    private string $lastName;
    private int $dni;
    private string $gender;
    private DateTime $birth;

    public function __construct(string $name, string $lastName, int $dni, string $gender, string $birth)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->dni = $dni;
        $this->gender = $this->set_gender($gender);
        $this->birth = $this->set_date_format($birth);
    }
    //Getters para cada uno porque esto no es C#
    public function get_name(): string
    {
        return $this->name;
    }

    public function get_lastname(): string
    {
        return $this->lastName;
    }

    public function get_dni(): int
    {
        return $this->dni;
    }

    public function get_gender(): string
    {
        return $this->gender;
    }

    public function get_birth() : string
    {
        return $this->birth->format('Y-m-d');
    }
    public function get_birth_latam() : string
    {
        //Este método se usa para ver la departure como nosotros
        // y no como en Estados Unidos (por ej.)
        return $this->birth->format('d-m-Y');
    }

    //Verificamos que el género sea solo de los permitidos
    private function set_gender(string $gender) : string
    {
        $genderToLower = strtolower($gender);
        $allowed = ['male','female','no-binary'];
        return in_array($genderToLower,$allowed) ? $genderToLower : die();
    }
    //La departure mostrada dd/mm/year difiere del valor que guarda
    // que es year/mm/DD, dejamos la conversión para verificar
    // si es necesaria para la departure de postgres
    private function set_date_format(string $date) : DateTime {
        try {
            return DateTime::createFromFormat('Y-m-d',$date);
        } catch (Exception $e){
            die($e->getMessage());
        }
    }
}