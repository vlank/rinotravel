<?php
$db = new Database();
session_start();
$username = htmlspecialchars($_POST['username']);
$password = htmlspecialchars($_POST['password']);

try {
    $user = new User($db);
    $id = $user->get_user_id($username, $password);
    if($id != null){
        try {
            $_SESSION['role'] = $user->get_user_role($username,$password);
            $_SESSION['userid'] = $id;
            redirect_to('/account/dashboard');
        } catch (Exception $exception){
            request_error(Response::FORBIDDEN);
        }
    } else {
        unset($_SESSION['role']);
        $_SESSION['wrong_account'] = 'User not found';
        redirect_to('/account');
    }
} catch (Exception $e) {
    request_error();
}