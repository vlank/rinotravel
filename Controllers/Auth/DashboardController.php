<?php
$title = "Dashboard";
$db = new Database();

session_start();


try {
    $user = new User($db);
    $userid = $_SESSION['userid'];
    if(isset($userid)){
        $username = $user->get_user_name($userid);
        require 'Views/Auth/dashboard.view.php';
    } else {
        request_error(403);
    }
} catch (Exception $exception){
    request_error();
}