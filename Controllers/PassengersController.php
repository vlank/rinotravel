<?php
$title = "Passengers";
session_start();
// We are going to redirect to this page, so we check the existence of the variable
if(!isset($_SESSION['ticket_id'])){
    $ticket_id = $_POST['ticket_id'];
    $_SESSION['ticket_id'] = $ticket_id;
}
$passengers = $_SESSION['passengers'];

require dirname(__DIR__) . '/Views/passengers.view.php';