<?php
$title = "RinoTravel";
session_start();
if(!isset($_SESSION['role'])){
    //In case of redirection without finalizing the process
    clean_session();
} else {
    $role = $_SESSION['role'];
    $userid = $_SESSION['userid'];
    clean_session();
    session_start();
    $_SESSION['userid'] = $userid;
    $_SESSION['role'] = $role;
}

try {
    $db = new Database();
    //Select loader
    try {
        $origin = $db->get_cities('origin');
        $destination = $db->get_cities('destination');
        $db->close();
        if($origin != null && $destination != null){
            //How many passengers will be loaded
            $passengers = [1,2,3,4,5,6];
            $columnName = "city";
            require 'Views/home.view.php';
        } else {
            http_response_code(502);
            echo "Error en los recursos";
        }
    } catch (Exception $exception){
        echo $exception->getMessage();
    }
} catch (Exception $exception){
    echo $exception->getMessage();
}





