<?php
$title = "Tickets";

$origin = strtolower($_POST['origin']);
$destination = strtolower($_POST['destination']);
$passengers = $_POST['passengers'];
$departure = $_POST['departure_date'];

try {
    $db = new Database();
    $tickets = $db->get_tickets($origin, $destination, $passengers, $departure);
    $db->close();
    session_start();
    $_SESSION['passengers'] = $passengers;
    require dirname(__DIR__) . '/Views/tickets.view.php';
} catch (Exception $e) {
    echo $e->getMessage();
}