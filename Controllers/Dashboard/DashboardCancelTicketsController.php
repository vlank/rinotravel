<?php
$title = "Cancellation";
session_start();
$ticket_id = $_POST['ticket_id'];

$rol = $_SESSION['role'];

if($rol != null){
    if ($rol === Role::ADMIN){
        try {
            $db = new Database();
            $user = new User($db);
            if($user->cancel_ticket($ticket_id)){
                require dirname(__DIR__,2).'/Views/Dashboard/dashboard_cancelation.view.php';
            } else {
                request_error(500);
            }
        } catch (Exception $exception){
            request_error();
        }
    } else {
        request_error(403);
    }

}
