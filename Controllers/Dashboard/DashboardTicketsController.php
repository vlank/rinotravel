<?php
$title = "My tickets";
session_start();
$db = new Database();
$userid = $_SESSION['userid'];
try {
    $user = new User($db);
    $tickets = $user->get_user_tickets($userid);
    require dirname(__DIR__,2).'/Views/Dashboard/dashboard_tickets.view.php';
} catch (Exception $e){
    request_error();
}

