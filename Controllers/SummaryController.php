<?php
$title = "Summary";

$db = new Database();

session_start();
$tickets_amount = (int)$_SESSION['passengers'];
$ticket_id = $_SESSION['ticket_id'];
$fields = get_field_names($tickets_amount);
$customers = array();

for ($x = 0; $x < $tickets_amount; $x++){
    $customers[] = new Customer(
        //Recuperamos los valores con su respectiva clave
        name: $_POST[$fields['name'][$x]],
        lastName: $_POST[$fields['lastname'][$x]],
        dni: $_POST[$fields['dni'][$x]],
        gender: $_POST[$fields['gender'][$x]],
        birth: $_POST[$fields['birth'][$x]]
    );
}

$price = $db->get_price($ticket_id) * $tickets_amount;


$db->close();
$counter = 0;

require dirname(__DIR__) . '/Views/summary.view.php';