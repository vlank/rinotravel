<?php
$title = "My account";
session_start();

if(isset($_SESSION['wrong_account'])){
    $wrong_account = $_SESSION['wrong_account'];
    require dirname(__DIR__) . '/Views/Auth/account.view.php';
} elseif (isset($_SESSION['userid'])){
    redirect_to('/account/dashboard');
} else {
    require dirname(__DIR__) . '/Views/Auth/account.view.php';
}