<?php 
$stylesrc = "tickets_layout.css";
require('Views/Partials/header.php'); 
?>
<div class="nav-bar">
    <div class="nav-items">
        <div class="emp-logo">
            <a href="/" title="Back to home">RinoTravel</a>
        </div>
        <a>Choose ticket</a>
    </div>
</div>
<div class="body-low">
    <?php if(!empty($tickets)):?>
        <?php foreach ($tickets as $ticket):?>
        <div class="pasajes">
            <form method="post" action="/passengers">
                <div class="pasaje-tipo">
                        <label for="ticket_id">Id
                            <input id="ticket_id" name="ticket_id" type="text" value="<?php echo $ticket['id']?>" readonly>
                        </label>
                    <h2><?php echo ucwords($ticket['type'])  ?></h2>
                </div>
                <div class="pasaje-horario">
                    <div class="pasaje-origen">
                        <div class="pasaje-text-1">Depart</div>
                        <div class="pasaje-text-2"><?php echo $ticket['departure_date']?></div>
                        <div class="pasaje-text-3"><?php echo get_time_latam($ticket['departure_time']).'hs'?></div>
                        <div class="pasaje-text-4"><?php echo ucwords($origin)?></div>
                    </div>
                    <div class="pasaje-tiempo">
                        <img src="/styles/icons/bus.svg" alt="clock">
                    </div>
                    <div class="pasaje-destino">
                        <div class="pasaje-text-1">Arrive</div>
                        <div class="pasaje-text-2"><?php echo $ticket['arrival_date']?></div>
                        <div class="pasaje-text-3"><?php echo get_time_latam($ticket['arrival_time']).'hs'?></div>
                        <div class="pasaje-text-4"><?php echo ucwords($destination)?></div>
                    </div>
                </div>
                <div class="pasaje-precio">
                    <div class="pasaje-boton">
                        <div class="precio-php">
                            $<?php echo $ticket['price'] ?>
                            <small>per person</small>
                        </div>
                        <input type="submit" class="button-pad" value="Buy">

                    </div>
                    <div class="asientos"><?php echo $ticket['available_capacity'];
                        if($ticket['available_capacity'] == 1){
                            echo " ticket";
                        } else {
                            echo " tickets";
                        }
                        ?> available</div>
                </div>
            </form>

        </div>
        <?php endforeach; ?>
    <?php else:?>
    <div class="pasajes-not-found">
        <h3>There are no tickets found for the selected origin, destination, number of passengers or date</h3>
        <a href="/"><button class="btn btn-submiteo">Back to home</button> </a>
    </div>
    <?php endif; ?>
</div>
<?php require('Views/Partials/footer.php');
