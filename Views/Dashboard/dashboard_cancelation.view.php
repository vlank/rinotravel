<?php require('Views/Partials/header.php')?>
<div class="nav-bar">
    <div class="nav-items">
        <div class="emp-logo">
            <a href="/" title="Back to home">RinoTravel</a>
        </div>
    </div>
</div>
<div class="body-low">
    <h1>Ticket cancelled</h1>
    <div class="dashboard-back-home">
        <a class="btn btn-submiteo" href="/">Back to home</a>
    </div>
</div>