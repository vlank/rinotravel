<?php 
$stylesrc = "dashboard_tickets.css";
require('Views/Partials/header.php');?>

<div class="nav-bar">
    <div class="nav-items">
        <div class="emp-logo">
            <a href="/" title="Back to home">RinoTravel</a>
        </div>
    </div>
</div>
<div class="body-low">
    <?php if(!empty($tickets)):?>
        <?php foreach ($tickets as $ticket):?>
            <div class="pasajes">
                <form onsubmit="return confirmCancellation()"  method="post" action="/account/dashboard/tickets/cancellation">
                    <div class="pasaje-tipo">
                        <label for="ticket_id">Id
                            <input id="ticket_id" name="ticket_id" type="text" value="<?php echo $ticket['id']?>" readonly>
                        </label>
                        <h2><?php echo ucwords($ticket['origin']).' - '.ucwords($ticket['destination'])  ?></h2>
                    </div>
                    <div class="pasaje-horario">
                        <div class="pasaje-origen">
                            <div class="pasaje-text-1">Depart</div>
                            <div class="pasaje-text-2"><?php echo $ticket['departure_date']?></div>
                            <div class="pasaje-text-3"><?php echo get_time_latam($ticket['departure_time']).'hs'?></div>
                        </div>
                        <div class="pasaje-tiempo">
                            <img src="/styles/icons/bus.svg" alt="clock">
                        </div>
                        <div class="pasaje-destino">
                            <div class="pasaje-text-1">Arrive</div>
                            <div class="pasaje-text-2"><?php echo $ticket['arrival_date']?></div>
                            <div class="pasaje-text-3"><?php echo get_time_latam($ticket['arrival_time']).'hs'?></div>
                        </div>
                    </div>
                    <div class="dashboard-pasaje-precio">
                        <div class="pasaje-boton">
                            <div class="precio-php">
                                $<?php echo $ticket['price'] ?>
                            </div>
                            <input type="submit" class="button-cancel-2" value="Cancel">
                        </div>
                    </div>
                </form>

            </div>
        <?php endforeach; ?>
    <?php else:?>
        <div class="pasajes-not-found">
            <h3>You haven't purchased any tickets</h3>
            <a class="button-home" href="/">Back to home</a>
        </div>
    <?php endif; ?>
    <div class="dashboard-back-home">
        <a class="button-home" href="/">Back to home</a>
    </div>
</div>
<script>
    function confirmCancellation() {
        return confirm("Are you sure you want to cancel the ticket?");
    }
</script>