<?php 
$stylesrc = "end_layout.css";
require('Views/Partials/header.php');
?>
    <div class="nav-bar">
        <div class="nav-items">
            <div class="emp-logo">
                <a href="/">RinoTravel</a>
            </div>
            <?php if($status == 0) :?>
                <a>Unsuccessful purchase</a>
            <?php else: ?>
                <a>Successful purchase</a>
            <?php endif; ?>
        </div>
    </div>
<div class="body-end">
    <div class="body-desc">
        <?php if($status == 0) :?>
            <h2>Failed</h2>
            <h3>A customer is already assigned to the DNI number provided and the information you entered does not match</h3>
        <?php else: ?>
            <h2>Success</h2>
            <h3>Thank you for your purchase</h3>
            <p>You have been logged out</p>
        <?php endif; ?>
    </div>
    <?php if($status == 0) :?>
    <div class="end-buttons">
        <a class="btn btn-submiteo-5" href="/passengers">Edit data</a>
        <a class="btn btn-submiteo" href="/">Back to home</a>
    </div>
    <?php else: ?>
        <a href="/"><button class="btn btn-submiteo">Back to home</button> </a>
    <?php endif; ?>
</div>

<?php require('Views/Partials/footer.php');
