<?php 
$stylesrc = "responses_layout.css";
require('Views/Partials/header.php');
?>
    <div class="nav-bar">
        <div class="nav-items">
            <div class="emp-logo">
                <a href="/" title="Back to home">RinoTravel</a>
            </div>
            <a><?php
                /**
                 * @noinspection PhpUndefinedVariableInspection
                 */
                echo $code?></a>
        </div>
    </div>
    <div class="body-low">
        <div class="responses-message">
                <h1>Error <?php echo $code?></h1>
                <p><?php
                    /**
                     * @noinspection PhpUndefinedVariableInspection
                     */
                    echo $message?></p>
            <a href="/" class="button-home">Back to home</a>
        </div>
    </div>
<?php require('Views/Partials/footer.php');
