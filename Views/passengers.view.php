<?php 
$stylesrc = "passengers_layout.css";
require('Views/Partials/header.php');
?>
    <div class="nav-bar">
        <div class="nav-items">
            <div class="emp-logo">
                <a href="/">RinoTravel</a>
            </div>
            <a>Enter passenger details</a>
        </div>
    </div>
<div class="body-low">
    <div class="personas-form">
        <!--Generamos una serie de campos a llenar por cada pasajero
        y le asignamos un número al nombre para diferenciarlos
        en la carga-->
        <form method="post" action="/summary">
            <?php for($x = 0; $x < (int)$_SESSION['passengers'];$x++):?>
            <h2>Passenger <?php echo $x + 1?></h2>
            <div class="clientes-fields">
                <label for="name<?php echo $x?>"> First name
                    <input id="name<?php echo $x?>" name="name<?php echo $x?>" type="text" required>
                </label>
                <label for="lastname<?php echo $x?>"> Last name
                    <input id="lastname<?php echo $x?>" name="lastname<?php echo $x?>" type="text" required>
                </label>
            </div>
            <div class="clientes-fields">
                <label for="dni<?php echo $x?>"> DNI
                    <input id="dni<?php echo $x?>" name="dni<?php echo $x?>" type="text"
                           minlength="7" maxlength="8" pattern="[0-9]+" required title="Only numbers allowed">
                </label>
                <label for="gender<?php echo $x?>">Gender
                    <select id="gender<?php echo $x?>" name="gender<?php echo $x?>" required>
                        <option selected disabled value="">Choose an option...</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="no-binary">No-binary</option>
                    </select>
                </label>
            </div>
            <div>
                <label for="birth<?php echo $x?>"> Date of birth
                    <input id="birth<?php echo $x?>" name="birth<?php echo $x?>" type="date" required>
                </label>
            </div>
            <?php endfor; ?>
            <div class="clientes-submit">
                <input type="submit" class="button-height" value="Confirm">
            </div>

        </form>
    </div>


</div>
<?php require('Views/Partials/footer.php');