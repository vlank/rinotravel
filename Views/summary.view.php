<?php 
$stylesrc = "summary_layout.css";
require('Views/Partials/header.php');
?>
    <div class="nav-bar">
        <div class="nav-items">
            <div class="emp-logo">
                <a href="/">RinoTravel</a>
            </div>
            <a>Summary</a>
        </div>
    </div>
<div class="body-low">
    <div class="compra-rect">
        <form class="compra-form" method="post" action="/end">
            <?php
            /**
             * @noinspection PhpUndefinedVariableInspection
             * */
            foreach ($customers as $customer) : ?>
                <div class="compra-pasajeros-container">
                    <div class="compra-pasajeros-id">
                        <label for="quantity">
                            <input id="quantity" name="quantity" type="text" value="Passenger <?php echo $counter + 1?>">
                        </label>

                    </div>
                    <div class="compra-pasajeros-fields">
                        <label for="name<?php echo $counter?>">
                            Name:
                            <input id="name<?php echo $counter?>" name="name<?php echo $counter?>" type="text" value="<?php echo ucwords($customer->get_name()) ?>" readonly>
                        </label>
                        <label for="lastname<?php echo $counter?>">
                            Last Name:
                            <input id="lastname<?php echo $counter?>" name="lastname<?php echo $counter?>" type="text" value="<?php echo ucwords($customer->get_lastname()) ?>" readonly>
                        </label>
                        <label>
                            DNI:
                            <input id="dni<?php echo $counter?>" name="dni<?php echo $counter?>" type="text" value="<?php echo $customer->get_dni() ?>" readonly>
                        </label>
                        <label for="gender<?php echo $counter?>">
                            Gender:
                            <input id="gender<?php echo $counter?>" name="gender<?php echo $counter ?>" type="text" value="<?php echo ucwords($customer->get_gender()) ?>" readonly>
                        </label>
                        <label for="birth<?php echo $counter ?>">
                            Date of birth:
                            <input id="birth<?php echo $counter ?>" name="birth<?php echo $counter ?>" type="text" value="<?php echo $customer->get_birth(); ?>" readonly>
                        </label>
                    </div>
                </div>
                <?php $counter++?>
            <?php endforeach;?>
            <!--Mostrar precio-->
            <div class="compra-pasajeros-precio">
                <h4>Final price $<?php
                    /**
                     * @noinspection PhpUndefinedVariableInspection
                     * */
                    echo $price; ?></h4>
            </div>
            <div class="compra-pasajeros-confirm">
                <button class="button-confirm" type="submit">Confirm</button>
            </div>
        </form>
        <div class="compra-pasajeros-edit">
        <a class="button-cancel" href="/passengers">Edit</a>
        </div>
    </div>
</div>

<?php require('Views/Partials/footer.php');