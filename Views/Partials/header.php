<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="A CRUD to tackle PHP development">
    <meta property="og:description" content="A CRUD to tackle PHP development">
    <meta property="og:title" content="
    <?php
    /**
     * @noinspection PhpUndefinedVariableInspection
     * */
    echo $title?>">
    <meta property="og:site_name" content="RinoTravel">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://rinotravel.azurewebsites.net/">
    <meta property="og:image" content="https://rinotravel.azurewebsites.net/styles/preview/preview.png" data-head-react="true">
    <meta property="og:image:width" content="1280">
    <meta property="og:image:height" content="720">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="/styles/fonts.css">

    <!-- <link rel="stylesheet" href="/styles/pasajes_layout.css">
    <link rel="stylesheet" href="/styles/clientes_layout.css">
    <link rel="stylesheet" href="/styles/compra_layout.css">
    <link rel="stylesheet" href="/styles/final_layout.css">
    <link rel="stylesheet" href="/styles/responses_layout.css">
    <link rel="stylesheet" href="/styles/auth_layout.css">
    <link rel="stylesheet" href="/styles/dashboard_layout.css"> -->
    <link rel="stylesheet" href="/styles/shared/container.css">
    <link rel="stylesheet" href="/styles/shared/style_reset.css">
    <link rel="stylesheet" href="/styles/shared/navbar_component.css">
    <link rel="stylesheet" href="/styles/shared/button_component.css">
    <link rel="stylesheet" href="/styles/<?=$stylesrc?>">
    <title><?php echo $title ?></title>
</head>
<body>