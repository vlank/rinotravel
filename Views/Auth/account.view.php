<?php 
$stylesrc = "auth_layout.css";
require('Views/Partials/header.php');
?>
<div class="nav-bar">
    <div class="nav-items">
        <div class="emp-logo">
            <a href="/" title="Back to home">RinoTravel</a>
        </div>
    </div>
</div>
<div class="login-body-low">
    <div class="login-container">
        <h2>Sign in</h2>
        <form class="login-form" method="post" action="/account/login">
            <div class="login-input">
                <label for="username">
                    <input name="username" placeholder="Username" type="text" value="preview" required>
                </label>
            </div>
            <div class="login-input">
                <label for="password">
                    <input name="password" placeholder="Password" type="password" value="seewhat" required>
                </label>
            </div>
            <div class="login-error">
                <p><?php if(isset($wrong_account)){
                        echo $wrong_account;
                    } ?>
                </p>
            </div>
            <div class="login-submit">
                <input type="submit" class="button-pad" value="Log in">
            </div>
        </form>
    </div>
</div>