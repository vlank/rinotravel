<?php 
$stylesrc = "dashboard_layout.css";
require('Views/Partials/header.php');
?>
<div class="nav-bar">
    <div class="dash-items">
        <div class="emp-logo">
            <a href="/" title="Back to home">RinoTravel</a>
        </div>
        <a class="logout" href="/account/logout">Logout</a>
    </div>
</div>
<div class="dashboard-body-low">
    <h1>Welcome, <?php echo $username?></h1>
    <div class="dashboard-menu">
        <div class="dashboard-panel">
            <a class="dashboard-item" href="/account/dashboard/tickets">
                <img src="/styles/icons/tickets-plane.svg" alt="tickets icon">
                <p>My tickets</p>
            </a>
        </div>
    </div>
    <a class="button-home" href="/">Back to home</a>
</div>