<?php 
$stylesrc = "home_layout.css";
require('Views/Partials/header.php');
?>
    <div class="nav-bar">
        <div class="nav-items">
            <div class="emp-logo">
                <a href="/">RinoTravel</a>
            </div>
            <div class="nav-links">
            <a title="Fake link">Plan travel</a>
            <a title="Fake link">Travel information</a>
            <a title="Fake link">Prices</a>

            
            </div>
            <a class="nav-account" href="/account">
                <img src="/styles/icons/account.svg" alt="account">
            </a>
        </div>
    </div>

    <div class="main-form">
        <div class="main-form-img">
            <img src="/styles/pexels-pok-rie-1726310.jpg" alt="marine">
        </div>
        <form class="home-form" method="post" action="/tickets">
            <div class="main-form-group">
                <label for="origin" class="form-labels">From
                    <select id="origin" name="origin" class="form-control form-control-sm">
                        <?php foreach ($origin as $ori): ?>
                            <option value="<?php echo ucwords($ori[$columnName]) ?>"><?php echo ucwords($ori[$columnName]) ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>

                <label for="destination" class="form-labels">To
                    <select id="destination" name="destination" class="form-control form-control-sm">
                        <?php foreach ($destination as $des): ?>
                            <option value="<?php echo ucwords($des[$columnName]) ?>"><?php echo ucwords($des[$columnName]) ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>

                <label for="passengers" class="form-labels">Nº Passengers
                    <select id="passengers" name="passengers" class="form-control form-control-sm" required>
                        <option selected disabled value="">--</option>
                        <?php foreach ($passengers as $cant): ?>
                            <option value="<?php echo $cant ?>"><?php echo $cant ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>

                <label for="departure_date" class="form-labels">Departure
                    <input id="departure_date" name="departure_date" type="date" value="2023-08-18"
                           title="Only tickets available for this date" required>
                </label>

                <input type="submit" class="button-height" value="Search">

            </div>
        </form>
    </div>
<?php require('Views/Partials/footer.php');