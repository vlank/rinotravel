<?php
/**
 * This function is used to generate the form names
 * available according to the number of tickets chosen
 * @param string $fieldName is the name of the field in the form
 * @param int $quantity is the number of tickets requested
 * @return array name0,name1,name2 (for each name at once)
 * */
function generate_dynamic_field_names(string $fieldName , int $quantity): array
{
    $allowed = ['name','lastname','dni','gender','birth'];
    $fullName = [];
    if(in_array($fieldName,$allowed)){
        for ($x = 0; $x < $quantity; $x++){
            $fullName[] = $fieldName.$x;
        }
        return $fullName;
    } else {
        die();
    }
}
function redirect_to($uri): void
{
    header("Location: $uri");
}

function clean_session() : void
{
    if(session_status() == PHP_SESSION_ACTIVE){
        session_unset();
        session_destroy();
    }
}
function set_date_format(string $date) : DateTime
// NOTE: Used to convert a string to date format
{
    try {
        return DateTime::createFromFormat('Y-m-d',$date);
    } catch (Exception $e){
        die($e->getMessage());
    }
}
function set_time_format(string $time) : DateTime
// NOTE: Used to convert a string to time format
{
    try {
        return DateTime::createFromFormat('H:i:s',$time);
    } catch (Exception $e){
        die($e->getMessage());
    }
}
function get_date_latam(string $date) : string
// NOTE: Used to display the date in our format
{
    return set_date_format($date)->format('d-m-Y');
}
function get_time_latam(string $time) : string
// NOTE: Displays the time as 00:00 hours
{
    return set_time_format($time)->format('H:i');
}
/**
 * This function groups all required field names into an array
 * @param int $quantity is the number of tickets requested
 * @return array with all names into an array
 */
function get_field_names(int $quantity): array
{
    $names = generate_dynamic_field_names("name",$quantity);
    $lastNames = generate_dynamic_field_names("lastname",$quantity);
    $dnis = generate_dynamic_field_names("dni",$quantity);
    $genders = generate_dynamic_field_names("gender",$quantity);
    $births = generate_dynamic_field_names("birth",$quantity);
    return [
        //name0, name1, name n...
        'name' => $names,
        //lastname0, lastname1, lastname n...
        'lastname' => $lastNames,
        'dni' => $dnis,
        'gender' => $genders,
        'birth' => $births
    ];
}

function request_error(int $code = Response::NOT_FOUND): void
{
    http_response_code($code);
    $_SESSION["response-code"] = $code;
    require 'Controllers/ResponsesController.php';
}