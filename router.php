<?php
//
$uri = parse_url($_SERVER['REQUEST_URI'])['path'];

$routes = [
    '/' => 'Controllers/HomeController.php',
    '/account' => 'Controllers/AccountController.php',
    '/account/dashboard' => 'Controllers/Auth/DashboardController.php',
    '/account/login' => 'Controllers/Auth/LoginController.php',
    '/account/logout' => 'Controllers/Auth/LogoutController.php',
    '/account/dashboard/tickets' => 'Controllers/Dashboard/DashboardTicketsController.php',
    '/account/dashboard/tickets/cancellation' => 'Controllers/Dashboard/DashboardCancelTicketsController.php',
    '/tickets' => 'Controllers/TicketsController.php',
    '/passengers' => 'Controllers/PassengersController.php',
    '/summary' => 'Controllers/SummaryController.php',
    '/end' => 'Controllers/EndController.php'
];

routeToController($uri,$routes);

function routeToController($uri, $routes) : void{
    if(array_key_exists($uri,$routes)){
        require $routes[$uri];
    } else {
        request_error();
    }
}
